from robot.get_info import *
from openexcel.excel import *
from mysql.sql import *
class Super(object):
    def save_grade(self,sid,pwd,file_name='成绩信息.xlsx'):
        #获取某个学生的成绩并将它保存到excel中
        info = Get_info(sid,pwd)
        info.get_person_info()
        # for key in info.person_data:
        #     print(key,':',info.person_data[key])
        grade = info.get_grade_info()
        title = [key for key in grade[1]]
        form = []
        form.append(title)
        for row in grade:
            content = [row[key] for key in row]
            form.append(content)
        excel = Excel(file_name,info.name)
        excel.insert_to_form(form)
        excel.save()
        excel.close()
        self.chart_by_grade(file_name, info.name)
    def save_person_data(self,sid,pwd,file_name='个人信息'):
        info = Get_info(sid, pwd)
        info.get_person_info()
        # for key in info.person_data:
        #     print(key,':',info.person_data[key])
        datas = info.get_person_info()
        title = [key for key in datas]
        form = []
        form.append(title)
        content=[datas[key] for key in datas]
        form.append(content)
        excel = Excel(file_name, info.name)
        excel.insert_to_form(form)
        excel.save()
    def chart_by_grade(self,file_name,sheet_name):
        #通过成绩生成图表
        sheet = Excel(file_name,sheet_name)
        sheet.make_chart('PieChart')
        sheet.chart_title = sheet_name+'的成绩数据'
        sheet.titles_from_data = 1
        sheet.add_title(min_col='1', min_row='2', max_row=sheet.end_row, max_col='1')
        sheet.add_data(min_col='2', min_row='2', max_row=sheet.end_row, max_col='2')
        sheet.insert_chart(op='column_end')
        sheet.save()